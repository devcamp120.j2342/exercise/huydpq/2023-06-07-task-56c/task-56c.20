package com.devcamp.task56c20.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c20.Service.ArtistService;
import com.devcamp.task56c20.models.Artist;

@RestController
@CrossOrigin
public class ArtistController {
    @Autowired
    private ArtistService artistService;
    @GetMapping("/artists")
    public ArrayList<Artist> getAlArrayList (){
        ArrayList <Artist> artist = artistService.getAllArtist();

        return artist;
    }

    @GetMapping("/artists-info")
    public Artist getArtistInfo (@RequestParam(required = true, name = "id") int artistId){
        Artist artists = artistService.fillteArtist(artistId);

        return artists;
    }
}
