package com.devcamp.task56c20.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task56c20.models.Album;

@Service
public class AlbumService {
    public ArrayList<String> createSongs1() {
        ArrayList<String> songs1 = new ArrayList<>();
        songs1.add("bai hat 1");
        songs1.add("bai hat 2");
        songs1.add("bai hat 3");
        return songs1;
    }

    public ArrayList<String> createSongs2() {
        ArrayList<String> songs2 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs2
        songs2.add("ten bai hat 1");
        songs2.add("ten bai hat 2");
        songs2.add("ten bai hat 3");
        return songs2;
    }

    public ArrayList<String> createSongs3() {
        ArrayList<String> songs3 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs3.add("ten bai hat 1");
        songs3.add("ten bai hat 2");
        songs3.add("ten bai hat 3");
        return songs3;
    }

    public ArrayList<String> createSongs4() {
        ArrayList<String> songs4 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs4.add("ten bai hat 1");
        songs4.add("ten bai hat 2");
        songs4.add("ten bai hat 3");
        return songs4;
    }
    public ArrayList<String> createSongs5() {
        ArrayList<String> songs5 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs5.add("ten bai hat 1");
        songs5.add("ten bai hat 2");
        songs5.add("ten bai hat 3");
        return songs5;
    }
    public ArrayList<String> createSongs6() {
        ArrayList<String> songs6 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs6.add("ten bai hat 1");
        songs6.add("ten bai hat 2");
        songs6.add("ten bai hat 3");
        return songs6;
    }

    public ArrayList<String> createSongs7() {
        ArrayList<String> songs7 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs7.add("ten bai hat 1");
        songs7.add("ten bai hat 2");
        songs7.add("ten bai hat 3");
        return songs7;
    }
    public ArrayList<String> createSongs8() {
        ArrayList<String> songs8 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs8.add("ten bai hat 1");
        songs8.add("ten bai hat 2");
        songs8.add("ten bai hat 3");
        return songs8;
    }
    public ArrayList<String> createSongs9() {
        ArrayList<String> songs9 = new ArrayList<>();
        // Thêm các tên bài hát vào danh sách songs3
        songs9.add("ten bai hat 1");
        songs9.add("ten bai hat 2");
        songs9.add("ten bai hat 3");
        return songs9;
    }


    Album album1 = new Album(01, "album1", createSongs1());
    Album album2 = new Album(02, "album2", createSongs2());
    Album album3 = new Album(03, "album3", createSongs3());

    Album album4 = new Album(04, "album4", createSongs4());
    Album album5 = new Album(05, "album5", createSongs5());
    Album album6 = new Album(06, "album6", createSongs6());

    Album album7 = new Album(07, "album7", createSongs7());
    Album album8 = new Album(12, "album8", createSongs8());
    Album album9 = new Album(13, "album9", createSongs9());

    public ArrayList<Album> getAllAlbumVol1(){
        ArrayList<Album> allAlbumVol1 = new ArrayList<>();
        allAlbumVol1.add(album1);
        allAlbumVol1.add(album2);
        allAlbumVol1.add(album3);
        return allAlbumVol1;
    }
    public ArrayList<Album> getAllAlbumVol2(){
        ArrayList<Album> allAlbumVol2 = new ArrayList<>();
        allAlbumVol2.add(album4);
        allAlbumVol2.add(album5);
        allAlbumVol2.add(album6);
        return allAlbumVol2;
    }
    public ArrayList<Album> getAllAlbumVol3(){
        ArrayList<Album> allAlbumVol3 = new ArrayList<>();
        allAlbumVol3.add(album7);
        allAlbumVol3.add(album8);
        allAlbumVol3.add(album9);
        return allAlbumVol3;
    }

    public Album getAlArrayListAlbum(int id){
        ArrayList<Album> allArrayListAlbum = new ArrayList<>();
        allArrayListAlbum.add(album1);
        allArrayListAlbum.add(album2);
        allArrayListAlbum.add(album3);
        allArrayListAlbum.add(album4);
        allArrayListAlbum.add(album5);
        allArrayListAlbum.add(album6);
        allArrayListAlbum.add(album7);
        allArrayListAlbum.add(album8);
        allArrayListAlbum.add(album9);
        Album album = new Album();
       for (Album albumElement : allArrayListAlbum) {
            if(albumElement.getId() == id){
                album = albumElement;
            }
       }
       return album;
    }
}
