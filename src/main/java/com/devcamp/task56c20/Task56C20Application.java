package com.devcamp.task56c20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56C20Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56C20Application.class, args);
	}

}
